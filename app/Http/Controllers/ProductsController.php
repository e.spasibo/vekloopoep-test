<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::query()->get();
        $resource = ProductResource::collection($products);

        return view('products.list')->with('data', $resource);
    }

    public function view($id)
    {
        $product = Product::query()->where('id', $id)->first();
        $resource = new ProductResource($product);

        return view('products.view')->with('data', $resource);
    }
}
