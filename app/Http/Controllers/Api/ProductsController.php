<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::query()->get();

        return ProductResource::collection($products);
    }

    public function view($id)
    {
        $product = Product::query()->where('id', $id)->first();

        return new ProductResource($product);
    }
}
