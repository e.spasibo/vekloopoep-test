<?php


namespace App\Http\Controllers\Api;


use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\OrderProductLink;
use App\Services\Pyments\PaymentsInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrdersController
{

    public function preview(Request $request, PaymentsInterface $service)
    {

        $data = [
            'products' => $request->get('products', []),
            'monthly' => $request->get('monthly', null)
        ];
        try {
            return new JsonResponse($service->preview($data));
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 400);
        }
    }

    public function create(Request $request, PaymentsInterface $service)
    {

        $order = Order::create([
            'payment_type' => get_class($service)
        ]);
        $order->refresh();
        foreach ($request->get('products', []) as $product) {
            OrderProductLink::create([
                'order_id' => $order->id,
                'product_id' => $product
            ]);
        }
        $service->store([
            'monthly_amount' => $request->get('monthly', null),
            'order_id' => $order->id
        ]);
        return new JsonResponse($order->toArray());
    }

    public function index() {
        $orders = Order::query()->get();

        return OrderResource::collection($orders);
    }

    public function view($id)
    {
        $order = Order::query()->where(['id' => $id])->first();

        if (empty($order)) {
            return new JsonResponse(['message' => 'no such order'], 404);
        }

        return new OrderResource($order);
    }
}