<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);
        $result['products'] = ProductResource::collection($this->products);
        $payment_service = new $this->payment_type();
        $products = [];
        foreach ($this->products as $product) {
            $products[] = $product->id;
        }
        $result['payments'] = $payment_service->preview([
            'products' => $products
        ], $this->id);
        return $result;
    }
}
