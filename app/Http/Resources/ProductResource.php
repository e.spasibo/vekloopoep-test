<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);
        $result['price_tax'] = floatval(number_format($this->price * Product::VAT, 2));
        $result['price_diff'] = floatval(number_format($result['price_tax'] - $result['price'], 2));
        return $result;
    }
}
