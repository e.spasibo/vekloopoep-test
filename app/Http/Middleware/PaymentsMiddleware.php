<?php

namespace App\Http\Middleware;

use App\Services\Pyments\PartialPayments;
use App\Services\Pyments\PaymentsInterface;
use App\Services\Pyments\SimplePayment;
use Closure;
use Illuminate\Http\Request;

class PaymentsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $classes = [
            'simple' => SimplePayment::class,
            'partial' => PartialPayments::class
        ];
        $selected = $request->get('payment_type', 'simple');
        app()->singleton(PaymentsInterface::class, $classes[$selected]);
        return $next($request);
    }
}
