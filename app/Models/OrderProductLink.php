<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderProductLink extends Model
{
    use HasFactory;

    protected $fillable = ['order_id', 'product_id'];

    protected $table = 'order_product';

    public $timestamps = false;
}
