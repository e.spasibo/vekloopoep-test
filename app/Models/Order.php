<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['payment_type'];

    public function products()
    {
        return $this->hasManyThrough(Product::class, OrderProductLink::class, 'order_id', 'id', 'id', 'product_id');
    }
}
