<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PartialPayments extends Model
{
    use HasFactory;

    protected $fillable = ['date_start', 'monthly_amount', 'order_id'];

    protected $table = 'partial_payments';
}
