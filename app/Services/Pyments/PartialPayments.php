<?php


namespace App\Services\Pyments;


use App\Models\Product;
use Illuminate\Http\Request;

class PartialPayments implements PaymentsInterface
{

    public function preview(array $data, $order_id = null)
    {
        $date = 'now';
        if (!empty($order_id)) {
            $payment = \App\Models\PartialPayments::query()->where(['order_id' => $order_id])->first()->toArray();
            $data['monthly'] = $payment['monthly_amount'];
            $date = $payment['date_start'];
        }
        $monthly = $data['monthly'];
        list($result, $months) = $this->calculate_sum($data['products'], $monthly);
        $now = new \DateTime($date);
        $data = [
            'total' => $result,
            'monthly' => [
                $now->format('m-Y') => $monthly
            ]
        ];
        $sub_tot = $monthly;
        for ($month = 1; $month <= $months; $month++) {
            if ($month == $months) {
                $val = (($result*100) - ($sub_tot*100)) / 100;
            } else {
                $val = $monthly;
                $sub_tot += $monthly;
            }
            $now->modify('+ 1 month');
            $data['monthly'][$now->format('m-Y')] = $val;
        }
        return $data;
    }

    public function calculate_sum(array $products, $monthly = null)
    {
        $result = 0;
        $products = Product::query()->whereIn('id', $products)->get();
        foreach ($products as $prod) {
            $result += $prod->price * Product::VAT;
        }
        $done = false;
        $interest = 4.1;
        $interest_index = 1.5;
        $check = $monthly * 100 / $result;
        if ($check < $interest_index) {
            throw new \Exception('Monthly amount too small, unpayable');
        }
        $months = floor($result / $monthly);
        do {
            $index = ($interest + ($interest_index * floor($months / 2))) / 100;
            $new_result = $result + ($result * $index);
            $new_months = floor($new_result / $monthly);
            if ($new_months == $months) {
                $done = true;
                $result = $new_result;
            }
            $months = $new_months;
        } while (!$done);
        return [number_format($result, 2), $months];
    }

    public function store(array $data)
    {
        \App\Models\PartialPayments::create([
            'date_start' => date('Y-m-d H:i:s'),
            'monthly_amount' => $data['monthly_amount'],
            'order_id' => $data['order_id']
        ]);
    }
}