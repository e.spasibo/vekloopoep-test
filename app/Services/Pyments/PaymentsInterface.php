<?php


namespace App\Services\Pyments;


use Illuminate\Http\Request;

interface PaymentsInterface
{

    public function preview(array $data, $order_id = null);
    public function calculate_sum(array $products, $monthly = null);
    public function store(array $data);
}