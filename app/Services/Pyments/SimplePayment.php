<?php


namespace App\Services\Pyments;


use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class SimplePayment implements PaymentsInterface
{

    public function preview(array $data, $order_id = null)
    {
        return ['total' => $this->calculate_sum($data['products'])['result']];
    }

    public function calculate_sum(array $products, $monthly = null)
    {
        $result = 0;
        $products = Product::query()->whereIn('id', $products)->get();
        foreach ($products as $prod) {
            $result += number_format($prod->price * Product::VAT, 2);
        }
        return ['result' => $result];
    }

    public function store(array $data)
    {
        // Nothing to store here!
        return;
    }
}