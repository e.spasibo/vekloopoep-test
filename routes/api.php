<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ProductsController;
use App\Http\Controllers\Api\OrdersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('products/list', [ProductsController::class, 'index']);
Route::get('product/{id}', [ProductsController::class, 'view']);
Route::group(['prefix' => 'orders', 'middleware' => 'payments'], function () {
    Route::get('list', [OrdersController::class, 'index']);
    Route::get('{id}', [OrdersController::class, 'view']);
    Route::post('preview', [OrdersController::class, 'preview']);
    Route::post('create', [OrdersController::class, 'create']);
});
