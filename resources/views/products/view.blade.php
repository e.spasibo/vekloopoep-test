@extends('layouts.app')

@section('content')
    <product-view :prod='@json($data)'></product-view>
@endsection
