@extends('layouts.app')

@section('content')
    <products-list :data='@json($data)'></products-list>
@endsection