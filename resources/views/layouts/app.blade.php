<html>
<head>
    <title>Animal food</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>

    <div id="app">
        @yield('content')
    </div>
    <script src="/js/app.js"></script>
</body>
</html>