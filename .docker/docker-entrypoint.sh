#!/bin/bash
cd /var/www/demo
php composer.phar install
php artisan key:generate
php artisan migrate
chown -R www-data storage/
nginx
php-fpm