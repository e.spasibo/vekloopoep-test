<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Bamboo',
                'price' => 115.25
            ],
            [
                'name' => 'Scorpions',
                'price' => 250.68
            ],
            [
                'name' => 'Pine cones',
                'price' => 46.12
            ]
        ];
        foreach ($data as $row) {
            Product::create($row);
        }
    }
}
